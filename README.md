Dog Breed and breed variant Sample
==============================================

This sample showcases the following Android Components:

* [Room](https://developer.android.com/topic/libraries/architecture/room.html)
* [ViewModels](https://developer.android.com/reference/android/arch/lifecycle/ViewModel.html)
* [LiveData](https://developer.android.com/reference/android/arch/lifecycle/LiveData.html)
* [MutableLiveData](https://developer.android.com/reference/android/arch/lifecycle/MutableLiveData)
* [DataBinding](https://developer.android.com/topic/libraries/data-binding)
* [RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview)
* [Retrofit](https://square.github.io/retrofit/)
* [Gson](https://github.com/google/gson)
* [RxJava](https://github.com/ReactiveX/RxAndroid)
* [Dagger](https://github.com/google/dagger)

## Language, Tools and Frameworks

Android Studio 4.1.1

Kotlin 1.4.32

Gradle 4.1.3

BuildToolsVersion 30.0.2

Introduction
-------------

### Features

This sample contains two screens.

1. Screen one displays the list of all dog breeds available.
    a. Clicking/scrolling the alphabet index makes the list quickly jump to a specific breed

    b. Clicking the row will take the user to second Screen


2. Second Screen - In essence displays random images of the breed

    a. a horizontally scrollable top bar displays all the breed variants if any

    b. by default "All Variants" is selected (meaning mix random pics of variants of the selected breed)

    c. selecting one of the top bar item will switch to specific variant images

    d. Use 'Fetch more' to request more images for the selected variant


### Limitations

1. Internet Connectivity errors are not propagated to UI layer. Therefore PLEASE CHECK INTERNET.
2. Fetch will limit the result to only 10 and new images will not get append to existing items.
3. Minimum SDK 21
4. Maximum SDK 30
5. Android Studio 4.1.1
