package com.timid.lupus.doggy.ui

import com.timid.lupus.doggy.SchedulerProvider
import com.timid.lupus.doggy.domain.usecase.GetAllDogBreedsUseCase
import com.timid.lupus.doggy.domain.usecase.SyncAllDogBreedsUseCase
import com.timid.lupus.doggy.ui.dog_breeds.DogBreedsViewModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class DogBreedsViewModelTest {

    private val schedulerProvider: SchedulerProvider = mock()
    private val getAllDogBreedsUseCase: GetAllDogBreedsUseCase = mock()
    private val syncAllDogBreedsUseCase: SyncAllDogBreedsUseCase = mock()

    private lateinit var viewModel: DogBreedsViewModel




    @Before
    fun setUp() {

        whenever(schedulerProvider.io())
            .thenReturn(Schedulers.trampoline())
        whenever(schedulerProvider.ui())
            .thenReturn(Schedulers.trampoline())

        whenever(getAllDogBreedsUseCase.observeAllDogBreeds())
            .thenReturn(Flowable.just(emptyList()))
        whenever(syncAllDogBreedsUseCase.syncAllDogBreeds())
            .thenReturn(Completable.complete())
    }

    @After
    fun tearDown() {

    }

    // @Test
    // fun `should return no dog breeds`() {
    //     configureViewModel()
    //
    //
    // }

    private fun configureViewModel() {
        viewModel = DogBreedsViewModel(
            schedulerProvider,
            getAllDogBreedsUseCase,
            syncAllDogBreedsUseCase
        )
    }
}