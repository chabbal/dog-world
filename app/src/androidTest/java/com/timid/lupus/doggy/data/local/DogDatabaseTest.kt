package com.timid.lupus.doggy.data.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.timid.lupus.doggy.data.local.dao.DogBreedDao
import com.timid.lupus.doggy.data.local.dao.DogBreedVariantDao
import com.timid.lupus.doggy.data.local.entities.DogBreedLocal
import com.timid.lupus.doggy.data.local.entities.DogBreedVariantLocal
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DogDatabaseTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()


    private lateinit var dogBreedDao: DogBreedDao
    private lateinit var dogBreedVariantDao: DogBreedVariantDao
    private lateinit var db: DogDatabase

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, DogDatabase::class.java).build()
        dogBreedDao = db.getDogBreedDao()
        dogBreedVariantDao = db.getDogBreedVariantDao()
    }

    @After
    fun tearDown() {
        db.close()
    }


    @Test
    fun testInsertAndReadResultSetWithThreeItemsAndOnlyOneBreedShouldHaveVariant() {

        val listOfBreeds = listOf(
            DogBreedLocal("German Shepherd"),
            DogBreedLocal("Bull Dog"),
            DogBreedLocal("Pointer"),

        )
        val dogBreedVariant = DogBreedVariantLocal(
            "Long Hair",
            "German Shepherd"
        )

        dogBreedDao.insertDogBreeds(listOfBreeds).blockingAwait()
        dogBreedVariantDao.insertDogBreedVariants(listOf(dogBreedVariant)).blockingAwait()
        dogBreedDao.getAllDogBreedsAndVariants()
            .test()
            .assertValue {
                it.size == 3 &&
                    it.filter { it.breedVariants.isNotEmpty() }.size == 1
            }
    }

    @Test
    fun testInsertAndShouldAbortWhenSamePrimaryKey() {

        val listOfBreeds = listOf(
            DogBreedLocal("German Shepherd"),
            DogBreedLocal("Bull Dog"),
            DogBreedLocal("Pointer"),

        )
        val listOfBreedVariants = listOf(
            DogBreedVariantLocal("Long Hair","German Shepherd"),
            DogBreedVariantLocal("Long Hair","German Shepherd")

        )

        dogBreedDao.insertDogBreeds(listOfBreeds).blockingAwait()
        dogBreedVariantDao.insertDogBreedVariants(listOfBreedVariants).blockingAwait()
        dogBreedDao.getAllDogBreedsAndVariants()
            .test()
            .assertValue {
                it.size == 3 &&
                    it.filter { it.breedVariants.isNotEmpty() }.size == 1
            }
    }

}