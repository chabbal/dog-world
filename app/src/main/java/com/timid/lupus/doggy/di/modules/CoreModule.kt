package com.timid.lupus.doggy.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class CoreModule(
    private val context: Context
) {

    @Provides
    fun providesApplicationContext(): Context = context
}