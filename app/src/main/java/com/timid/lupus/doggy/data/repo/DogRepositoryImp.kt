package com.timid.lupus.doggy.data.repo

import com.timid.lupus.doggy.data.local.dao.DogBreedDao
import com.timid.lupus.doggy.data.local.dao.DogBreedVariantDao
import com.timid.lupus.doggy.data.local.entities.DogBreedLocal
import com.timid.lupus.doggy.data.local.entities.DogBreedVariantLocal
import com.timid.lupus.doggy.data.remote.DogService
import com.timid.lupus.doggy.data.remote.responses.DogBreedResponse
import com.timid.lupus.doggy.domain.model.BreedImagesDomain
import com.timid.lupus.doggy.domain.model.DogBreedDomain
import com.timid.lupus.doggy.domain.model.toDogBreedDomainList
import com.timid.lupus.doggy.domain.repo.DogRepo
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class DogRepositoryImp @Inject constructor(
    private val dogService: DogService,
    private val dogBreedDao: DogBreedDao,
    private val dogBreedVariantDao: DogBreedVariantDao,
) : DogRepo {

    override fun observeAllDogBreeds(): Flowable<List<DogBreedDomain>> {
        return dogBreedDao.getAllDogBreedsAndVariants()
            .map { it.toDogBreedDomainList() }
    }

    override fun syncAllDogBreeds(): Completable {
        return dogService.getDogBreeds()
            .flatMapCompletable {
                val dogBreedsLocal = mutableListOf<DogBreedLocal>()
                val dogBreedsVariantLocal = mutableListOf<DogBreedVariantLocal>()
                it.dogBreeds.forEach { parentBreed ->
                    dogBreedsLocal.add(DogBreedLocal(parentBreed.name))
                    dogBreedsVariantLocal.addAll(getSubBreedsLocal(parentBreed))
                }
                dogBreedDao.insertDogBreeds(dogBreedsLocal)
                    .andThen(dogBreedVariantDao.insertDogBreedVariants(dogBreedsVariantLocal))
            }
    }

    override fun getRandomBreedImages(breed: String): Single<BreedImagesDomain> {
        return dogService.getRandomBreedImages(breed).map {
            BreedImagesDomain(it.images)
        }
    }

    override fun getRandomBreedVariantImages(
        breed: String,
        variant: String
    ): Single<BreedImagesDomain> {
        return dogService.getRandomBreedVariantImages(breed, variant).map {
            BreedImagesDomain(it.images)
        }
    }

    private fun getSubBreedsLocal(parentBreed: DogBreedResponse): List<DogBreedVariantLocal> {
        return parentBreed.subBreeds.map {
            DogBreedVariantLocal(
                it,
                parentBreed.name
            )
        }
    }
}