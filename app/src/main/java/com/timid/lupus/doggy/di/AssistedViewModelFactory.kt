package com.timid.lupus.doggy.di

import com.timid.lupus.doggy.ui.breed_images.RandomBreedImageViewModel
import com.timid.lupus.doggy.ui.model.DogBreed
import dagger.assisted.AssistedFactory

@AssistedFactory
interface AssistedViewModelFactory {
    fun createRandomBreedImageViewModelFactory(
        breed: DogBreed,
        allVariantKey: String
    ): RandomBreedImageViewModel.Factory
}