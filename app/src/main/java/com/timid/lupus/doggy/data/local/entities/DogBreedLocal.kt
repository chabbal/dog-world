package com.timid.lupus.doggy.data.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(tableName = "dog_breed")
data class DogBreedLocal(
    @PrimaryKey
    val name: String
)


data class BreedVariant(
    val variantName: String
)


data class DogBreedAndVariants(
    val name: String,
    @Relation(parentColumn = "name", entityColumn = "parentBreed", entity = DogBreedVariantLocal::class)
    val breedVariants: List<BreedVariant>
)

