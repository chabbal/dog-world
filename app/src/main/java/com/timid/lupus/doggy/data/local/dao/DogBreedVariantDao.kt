package com.timid.lupus.doggy.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.timid.lupus.doggy.data.local.entities.DogBreedVariantLocal
import io.reactivex.Completable

@Dao
interface DogBreedVariantDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDogBreedVariants(variants: List<DogBreedVariantLocal>): Completable
}