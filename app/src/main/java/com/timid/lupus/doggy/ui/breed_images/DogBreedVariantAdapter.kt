package com.timid.lupus.doggy.ui.breed_images

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.timid.lupus.doggy.R
import com.timid.lupus.doggy.databinding.ItemDogBreedVariantBinding
import com.timid.lupus.doggy.ui.model.BreedVariant

class DogBreedVariantAdapter: ListAdapter<BreedVariant, DogBreedVariantAdapter.ViewHolder>(VariantDiffUtilCallBack()) {

    private var itemClickListener: ItemClickListener? = null

    class ViewHolder(private val binding: ItemDogBreedVariantBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(variant: BreedVariant, itemClickListener: ItemClickListener?) {
            binding.variant = variant.name
            binding.isSelected = variant.isSelected
            binding.itemClickListener = itemClickListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_dog_breed_variant, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), itemClickListener)
    }

    fun setItemClickListener(listener: ItemClickListener) {
        itemClickListener = listener
    }

    interface ItemClickListener {
        fun onItemClickListener(variant: String)
    }
}

class VariantDiffUtilCallBack: DiffUtil.ItemCallback<BreedVariant>() {

    override fun areItemsTheSame(oldItem: BreedVariant, newItem: BreedVariant): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: BreedVariant, newItem: BreedVariant): Boolean {
        return oldItem.isSelected == newItem.isSelected
    }
}