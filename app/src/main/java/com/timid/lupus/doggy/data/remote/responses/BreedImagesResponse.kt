package com.timid.lupus.doggy.data.remote.responses

import com.google.gson.annotations.SerializedName

data class BreedImagesResponse(
    @SerializedName("message")
    val images: List<String>
)
