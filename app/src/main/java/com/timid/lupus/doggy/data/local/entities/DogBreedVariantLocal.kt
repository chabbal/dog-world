package com.timid.lupus.doggy.data.local.entities

import androidx.room.Entity

@Entity(
    tableName = "dog_breed_variant",
    primaryKeys = ["variantName", "parentBreed"]
)
data class DogBreedVariantLocal(
    val variantName: String,
    val parentBreed: String
)
