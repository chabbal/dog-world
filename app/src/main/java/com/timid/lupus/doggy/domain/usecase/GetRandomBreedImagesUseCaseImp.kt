package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.model.BreedImagesDomain
import com.timid.lupus.doggy.domain.repo.DogRepo
import io.reactivex.Single
import javax.inject.Inject

class GetRandomBreedImagesUseCaseImp @Inject constructor(
    private val repo: DogRepo
): GetRandomBreedImagesUseCase {
    override fun getRandomBreedImages(breed: String): Single<BreedImagesDomain> {
        return repo.getRandomBreedImages(breed)
    }
}