package com.timid.lupus.doggy.di

import com.timid.lupus.doggy.MainActivity
import com.timid.lupus.doggy.di.modules.CoreModule
import com.timid.lupus.doggy.di.modules.DataModule
import com.timid.lupus.doggy.di.modules.DomainModule
import com.timid.lupus.doggy.ui.breed_images.RandomBreedImageListActivity
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        CoreModule::class,
        DataModule::class,
        DomainModule::class
    ]
)
@Singleton
interface DogAppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(activity: RandomBreedImageListActivity)
}