package com.timid.lupus.doggy.di.modules

import android.content.Context
import com.google.gson.Gson
import com.timid.lupus.doggy.data.local.DogDatabase
import com.timid.lupus.doggy.data.local.dao.DogBreedDao
import com.timid.lupus.doggy.data.local.dao.DogBreedVariantDao
import com.timid.lupus.doggy.data.remote.DogService
import com.timid.lupus.doggy.data.repo.DogRepositoryImp
import com.timid.lupus.doggy.domain.repo.DogRepo
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataModule(
    private val baseUrl: String,
    private val isInDebugMode: Boolean,
    private val gson: Gson
) {



    @Provides
    fun providesDogService(retrofit: Retrofit): DogService {
        return retrofit.create(DogService::class.java)
    }

    @Provides
    fun providesDogRepository(dogService: DogService, dogBreedDao: DogBreedDao, dogBreedVariantDao: DogBreedVariantDao): DogRepo {
        return DogRepositoryImp(dogService, dogBreedDao, dogBreedVariantDao)
    }


    @Provides
    fun providesDogBreedDao(dogDatabase: DogDatabase): DogBreedDao {
        return dogDatabase.getDogBreedDao()
    }

    @Provides
    fun providesDogBreedVariantDao(dogDatabase: DogDatabase): DogBreedVariantDao {
        return dogDatabase.getDogBreedVariantDao()
    }

    @Singleton
    @Provides
    fun providesDatabase(context: Context): DogDatabase {
        return DogDatabase.createDatabase(context)
    }


    @Provides
    fun providesRetrofit(): Retrofit {
        val clientBuilder = OkHttpClient.Builder()
            .cache(null)

        if (isInDebugMode) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
            clientBuilder.addInterceptor(httpLoggingInterceptor)
        }

        val client = clientBuilder
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)

        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
        return builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }
}