package com.timid.lupus.doggy.ui.breed_images

import android.widget.ImageView
import android.widget.Toolbar
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("bindUrlImage")
fun bindUrlImage(view: ImageView, url: String) {
    Glide.with(view)
        .load(url)
        .apply(RequestOptions().override(view.width, view.height))
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(view)
}

@BindingAdapter("capitalizeTitle")
fun Toolbar.capitalizeTitle(title: String) {
    this.title = title.capitalize()
}