package com.timid.lupus.doggy.ui.model

data class BreedVariant(
    val name: String,
    var isSelected: Boolean = false
)
