package com.timid.lupus.doggy.di.modules

import com.timid.lupus.doggy.domain.repo.DogRepo
import com.timid.lupus.doggy.domain.usecase.GetAllDogBreedsUseCase
import com.timid.lupus.doggy.domain.usecase.GetAllDogBreedsUseCaseImp
import com.timid.lupus.doggy.domain.usecase.GetRandomBreedImagesUseCase
import com.timid.lupus.doggy.domain.usecase.GetRandomBreedImagesUseCaseImp
import com.timid.lupus.doggy.domain.usecase.GetRandomBreedVariantImagesUseCase
import com.timid.lupus.doggy.domain.usecase.GetRandomBreedVariantImagesUseCaseImp
import com.timid.lupus.doggy.domain.usecase.SyncAllDogBreedsUseCase
import com.timid.lupus.doggy.domain.usecase.SyncAllDogBreedsUseCaseImp
import dagger.Module
import dagger.Provides

@Module
class DomainModule {


    @Provides
    fun providesGetAllDogBreedUseCase(dogRepo: DogRepo): GetAllDogBreedsUseCase {
        return GetAllDogBreedsUseCaseImp(dogRepo)
    }


    @Provides
    fun providesSyncAllDogBreedsUseCase(dogRepo: DogRepo): SyncAllDogBreedsUseCase {
        return SyncAllDogBreedsUseCaseImp(dogRepo)
    }

    @Provides
    fun providesGetRandomBreedImagesUseCase(dogRepo: DogRepo): GetRandomBreedImagesUseCase {
        return GetRandomBreedImagesUseCaseImp(dogRepo)
    }

    @Provides
    fun providesGetRandomBreedVariantImagesUseCase(dogRepo: DogRepo): GetRandomBreedVariantImagesUseCase {
        return GetRandomBreedVariantImagesUseCaseImp(dogRepo)
    }
}