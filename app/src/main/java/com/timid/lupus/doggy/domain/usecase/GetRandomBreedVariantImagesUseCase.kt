package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.model.BreedImagesDomain
import io.reactivex.Single

interface GetRandomBreedVariantImagesUseCase {
    fun getRandomBreedVariantImages(breed: String, variant: String): Single<BreedImagesDomain>
}