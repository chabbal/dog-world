package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.model.BreedImagesDomain
import com.timid.lupus.doggy.domain.repo.DogRepo
import io.reactivex.Single
import javax.inject.Inject

class GetRandomBreedVariantImagesUseCaseImp @Inject constructor(
    private val repo: DogRepo
): GetRandomBreedVariantImagesUseCase {

    override fun getRandomBreedVariantImages(
        breed: String,
        variant: String
    ): Single<BreedImagesDomain> {
        return repo.getRandomBreedVariantImages(breed, variant)
    }
}