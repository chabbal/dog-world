package com.timid.lupus.doggy.ui.breed_images

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.timid.lupus.doggy.SchedulerProvider
import com.timid.lupus.doggy.domain.usecase.GetRandomBreedImagesUseCase
import com.timid.lupus.doggy.domain.usecase.GetRandomBreedVariantImagesUseCase
import com.timid.lupus.doggy.ui.RxJavaViewModel
import com.timid.lupus.doggy.ui.model.BreedVariant
import com.timid.lupus.doggy.ui.model.DogBreed
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class RandomBreedImageViewModel(
    private val schedulerProvider: SchedulerProvider,
    private val breedImagesUseCase: GetRandomBreedImagesUseCase,
    private val breedVariantImagesUseCase: GetRandomBreedVariantImagesUseCase,
    private val breed: DogBreed,
    private val allVariantKey: String
): RxJavaViewModel() {


    val breedName = breed.name.capitalize()
    var selectedVariant = allVariantKey

    private val breedImages = MutableLiveData<List<String>>()
    val breedImagesLiveData = breedImages as LiveData<List<String>>

    private val breedVariants = MutableLiveData<List<BreedVariant>>()
    val breedVariantsLiveData = breedVariants as LiveData<List<BreedVariant>>

    init {
        getVariantsIfAvailable()
        getRandomBreedImages()
    }

    private fun getVariantsIfAvailable() {
        if (breed.breedVariants.isNotEmpty()) {
            val breedVariantList = mutableListOf<BreedVariant>()
            breedVariantList.add(
                BreedVariant(allVariantKey, true)
            )
            breedVariantList.addAll(breed.breedVariants.map { BreedVariant(name = it) })

            breedVariants.value = breedVariantList
        }
    }

    fun getRandomImages(variant: String) {
        if (selectedVariant != variant) {
            updateDataSet(variant)
            selectedVariant = variant
            getImages()
        }
    }

    fun loadMoreRandom() {
        getImages()
    }

    private fun updateDataSet(incomingSelection: String) {
        breedVariants.value?.let {
            val newList = it.map { it.copy(isSelected = it.name == incomingSelection)}
            breedVariants.value = newList
        }
    }

    private fun getImages() {
        if (selectedVariant == allVariantKey) {
            getRandomBreedImages()
        } else {
            getRandomBreedVariantImages(selectedVariant)
        }
    }

    private fun getRandomBreedImages() {
        breedImagesUseCase.getRandomBreedImages(breed.name)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onSuccess = {
                    breedImages.value = it.images
                },
                onError = {

                }
            ).addTo(compositeDisposable)
    }

    private fun getRandomBreedVariantImages(variant: String) {
        breedVariantImagesUseCase.getRandomBreedVariantImages(breed.name, variant)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onSuccess = {
                    breedImages.value = it.images
                },
                onError = {

                }
            ).addTo(compositeDisposable)

    }




    class Factory @AssistedInject constructor(
        private val schedulerProvider: SchedulerProvider,
        private val getRandomBreedImagesUseCase: GetRandomBreedImagesUseCase,
        private val getRandomBreedVariantImages: GetRandomBreedVariantImagesUseCase,
        @Assisted private val breed: DogBreed,
        @Assisted private val allVariantKey: String
    ): ViewModelProvider.Factory{

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RandomBreedImageViewModel(
                schedulerProvider,
                getRandomBreedImagesUseCase,
                getRandomBreedVariantImages,
                breed,
                allVariantKey
            ) as T
        }
    }
}