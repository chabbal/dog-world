package com.timid.lupus.doggy.domain.usecase

import io.reactivex.Completable

interface SyncAllDogBreedsUseCase {
    fun syncAllDogBreeds(): Completable
}