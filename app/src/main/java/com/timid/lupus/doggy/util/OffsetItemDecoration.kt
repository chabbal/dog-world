package com.timid.lupus.doggy.util

import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import com.timid.lupus.doggy.R

open class OffsetItemDecoration(
    @DimenRes private val left: Int = R.dimen.item_offset_zero,
    @DimenRes private val top: Int = R.dimen.item_offset_zero,
    @DimenRes private val right: Int = R.dimen.item_offset_zero,
    @DimenRes private val bottom: Int = R.dimen.item_offset_zero
) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: androidx.recyclerview.widget.RecyclerView,
        state: androidx.recyclerview.widget.RecyclerView.State
    ) {
        outRect.left = parent.resources.getDimensionPixelSize(left)
        outRect.top = parent.resources.getDimensionPixelSize(top)
        outRect.right = +parent.resources.getDimensionPixelSize(right)
        outRect.bottom = +parent.resources.getDimensionPixelSize(bottom)
    }
}
