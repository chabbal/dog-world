package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.repo.DogRepo
import io.reactivex.Completable
import javax.inject.Inject

class SyncAllDogBreedsUseCaseImp @Inject constructor(
    private val dogRepo: DogRepo
): SyncAllDogBreedsUseCase {
    override fun syncAllDogBreeds(): Completable {
        return dogRepo.syncAllDogBreeds()
    }
}