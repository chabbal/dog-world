package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.model.DogBreedDomain
import io.reactivex.Flowable

interface GetAllDogBreedsUseCase {

    fun observeAllDogBreeds(): Flowable<List<DogBreedDomain>>
}