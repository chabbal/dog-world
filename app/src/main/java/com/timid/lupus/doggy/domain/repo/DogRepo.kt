package com.timid.lupus.doggy.domain.repo

import com.timid.lupus.doggy.domain.model.BreedImagesDomain
import com.timid.lupus.doggy.domain.model.DogBreedDomain
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface DogRepo {

    fun observeAllDogBreeds(): Flowable<List<DogBreedDomain>>

    fun syncAllDogBreeds(): Completable

    fun getRandomBreedImages(breed: String): Single<BreedImagesDomain>

    fun getRandomBreedVariantImages(breed: String, variant: String): Single<BreedImagesDomain>
}