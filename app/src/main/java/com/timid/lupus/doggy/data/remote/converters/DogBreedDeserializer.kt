package com.timid.lupus.doggy.data.remote.converters

import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.timid.lupus.doggy.data.remote.responses.DogBreedListResponse
import com.timid.lupus.doggy.data.remote.responses.DogBreedResponse
import java.lang.reflect.Type

class DogBreedDeserializer : JsonDeserializer<DogBreedListResponse> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): DogBreedListResponse {
        val messageObject = json?.asJsonObject?.getAsJsonObject("message")
        val listOfBreeds = mutableListOf<DogBreedResponse>()
        messageObject?.keySet()?.forEach {
            listOfBreeds.add(DogBreedResponse(it, getSubBreeds(messageObject.getAsJsonArray(it))))
        }

        return DogBreedListResponse(listOfBreeds)
    }

    private fun getSubBreeds(array: JsonArray): List<String> {
        val subBreeds = mutableListOf<String>()
        array.forEach {
            subBreeds.add(it.asString)
        }
        return subBreeds
    }
}