package com.timid.lupus.doggy.domain.model

import com.timid.lupus.doggy.data.local.entities.DogBreedAndVariants

data class DogBreedDomain(
    val name: String,
    val breedVariants: List<String>
)


fun DogBreedAndVariants.toDogBreedDomain() = DogBreedDomain(
    name,
    breedVariants = breedVariants.map { it.variantName }
)


fun List<DogBreedAndVariants>.toDogBreedDomainList() = this.map { it.toDogBreedDomain() }