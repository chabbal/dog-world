package com.timid.lupus.doggy.data.remote

import com.timid.lupus.doggy.data.remote.responses.BreedImagesResponse
import com.timid.lupus.doggy.data.remote.responses.DogBreedListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface DogService {

    @GET("breeds/list/all")
    fun getDogBreeds(): Single<DogBreedListResponse>

    @GET("breed/{breed_name}/images/random/10")
    fun getRandomBreedImages(
        @Path("breed_name", encoded = true) breed: String
    ): Single<BreedImagesResponse>

    @GET("breed/{breed_name}/{variant}/images/random/10")
    fun getRandomBreedVariantImages(
        @Path("breed_name", encoded = true) breed: String,
        @Path("variant", encoded = true) variant: String
    ): Single<BreedImagesResponse>
}