package com.timid.lupus.doggy

import android.app.Application
import com.google.gson.GsonBuilder
import com.timid.lupus.doggy.data.remote.converters.DogBreedDeserializer
import com.timid.lupus.doggy.data.remote.responses.DogBreedListResponse
import com.timid.lupus.doggy.di.DaggerDogAppComponent
import com.timid.lupus.doggy.di.DogAppComponent
import com.timid.lupus.doggy.di.modules.CoreModule
import com.timid.lupus.doggy.di.modules.DataModule
import com.timid.lupus.doggy.di.modules.DomainModule

class DogApplication: Application() {

    val appComponent: DogAppComponent = DaggerDogAppComponent.builder()
        .dataModule(
            DataModule(
                "https://dog.ceo/api/",
                BuildConfig.DEBUG,
                GsonBuilder()
                .registerTypeAdapter(DogBreedListResponse::class.java, DogBreedDeserializer())
                .create()
            )
        )
        .domainModule(DomainModule())
        .coreModule(CoreModule(this))
        .build()
}