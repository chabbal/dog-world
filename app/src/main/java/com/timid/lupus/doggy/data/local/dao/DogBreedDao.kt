package com.timid.lupus.doggy.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.timid.lupus.doggy.data.local.entities.DogBreedAndVariants
import com.timid.lupus.doggy.data.local.entities.DogBreedLocal
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface DogBreedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDogBreeds(breeds: List<DogBreedLocal>): Completable

    @Query("SELECT * FROM dog_breed")
    fun getAllDogBreeds(): Flowable<List<DogBreedLocal>>

    @Transaction
    @Query("SELECT * FROM dog_breed")
    fun getAllDogBreedsAndVariants(): Flowable<List<DogBreedAndVariants>>
}