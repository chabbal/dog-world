package com.timid.lupus.doggy.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.timid.lupus.doggy.data.local.dao.DogBreedDao
import com.timid.lupus.doggy.data.local.dao.DogBreedVariantDao
import com.timid.lupus.doggy.data.local.entities.DogBreedLocal
import com.timid.lupus.doggy.data.local.entities.DogBreedVariantLocal

@Database(
    version = 1,
    entities = [
        DogBreedLocal::class,
        DogBreedVariantLocal::class
    ],
    exportSchema = false
)
abstract class DogDatabase: RoomDatabase() {


    abstract fun getDogBreedDao(): DogBreedDao

    abstract fun getDogBreedVariantDao(): DogBreedVariantDao


    companion object {

        fun createDatabase(context: Context): DogDatabase {
            return Room.databaseBuilder(context, DogDatabase::class.java, "dog.db")
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}