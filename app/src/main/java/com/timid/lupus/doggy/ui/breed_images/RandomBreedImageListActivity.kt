package com.timid.lupus.doggy.ui.breed_images

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.timid.lupus.doggy.DogApplication
import com.timid.lupus.doggy.R
import com.timid.lupus.doggy.databinding.ActivityRandomBreedImageListBinding
import com.timid.lupus.doggy.di.AssistedViewModelFactory
import com.timid.lupus.doggy.ui.model.DogBreed
import com.timid.lupus.doggy.util.OffsetItemDecoration
import javax.inject.Inject

class RandomBreedImageListActivity : AppCompatActivity(), DogBreedVariantAdapter.ItemClickListener {

    companion object {
        const val EXTRAS_KEY = "breed"
    }

    @Inject
    lateinit var assistedFactory: AssistedViewModelFactory

    private val viewModel by lazy {
        ViewModelProvider(
            this,
            assistedFactory.createRandomBreedImageViewModelFactory(
                intent.getSerializableExtra(EXTRAS_KEY) as DogBreed,
                resources.getString(R.string.all_breed_variants)
            )
        ).get(RandomBreedImageViewModel::class.java)
    }

    private lateinit var binding: ActivityRandomBreedImageListBinding

    private val imageAdapter = BreedImageAdapter()
    private val variantAdapter = DogBreedVariantAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (applicationContext as DogApplication).appComponent.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_random_breed_image_list)
        binding.title = viewModel.breedName
        configureViews()
        setListeners()
        observeViewModel()
    }

    private fun configureViews() {
        binding.breedImages.apply {
            adapter = imageAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(
                OffsetItemDecoration(
                    top = R.dimen.item_offset_default,
                    bottom = R.dimen.item_offset_default
                )
            )
        }
        binding.breedVariants.apply {
            adapter = variantAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            addItemDecoration(
                OffsetItemDecoration(
                    top = R.dimen.item_offset_default,
                    left = R.dimen.item_offset_default,
                    right = R.dimen.item_offset_default
                )
            )
        }
    }

    private fun setListeners() {
        variantAdapter.setItemClickListener(this)

        binding.toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.menu_load_random) {
                viewModel.loadMoreRandom()

            }
            true
        }
    }

    private fun observeViewModel() {
        viewModel.breedImagesLiveData.observe(this) {
            imageAdapter.submitList(it)
        }
        viewModel.breedVariantsLiveData.observe(this) {
            variantAdapter.submitList(it.map { it.copy() })
        }
    }

    override fun onItemClickListener(variant: String) {
        viewModel.getRandomImages(variant)
    }
}