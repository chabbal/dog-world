package com.timid.lupus.doggy.data.remote.responses

data class DogBreedListResponse(
    val dogBreeds: List<DogBreedResponse>
)