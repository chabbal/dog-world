package com.timid.lupus.doggy

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.timid.lupus.doggy.databinding.ActivityMainBinding
import com.timid.lupus.doggy.ui.breed_images.RandomBreedImageListActivity
import com.timid.lupus.doggy.ui.custom_view.FastScrollIndexSelectionListener
import com.timid.lupus.doggy.ui.dog_breeds.DogBreedAdapter
import com.timid.lupus.doggy.ui.dog_breeds.DogBreedsViewModel
import com.timid.lupus.doggy.ui.model.DogBreed
import com.timid.lupus.doggy.util.OffsetItemDecoration
import javax.inject.Inject

class MainActivity : AppCompatActivity(), DogBreedAdapter.ItemClickListener,
    FastScrollIndexSelectionListener {

    @Inject
    lateinit var factory: DogBreedsViewModel.Factory

    private val viewModel by lazy {
        ViewModelProvider(this, factory).get(DogBreedsViewModel::class.java)
    }

    private val breedAdapter = DogBreedAdapter()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        (applicationContext as DogApplication).appComponent.inject(this)
        configureViews()
        observeViewModel()
    }


    private fun configureViews() {
        breedAdapter.setItemClickListener(this)
        binding.dogBreeds.apply {
            adapter = breedAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(
                OffsetItemDecoration(
                    top = R.dimen.item_offset_default,
                    bottom = R.dimen.item_offset_default
                )
            )
        }

        binding.fastScrollIndex.bubbleLayout = binding.bubbleLayout
        binding.fastScrollIndex.indexSelectionListener = this
    }

    private fun observeViewModel() {
        viewModel.dogBreedsLiveData.observe(this) {
            binding.isIndexVisible = it.isNotEmpty()
            breedAdapter.submitList(it)
        }
        viewModel.fastScrollPositionLiveData.observe(this) {
            (binding.dogBreeds.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(it, 0)
        }
    }

    override fun onItemClickListener(dogBreed: DogBreed) {
        val intent = Intent(this, RandomBreedImageListActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(RandomBreedImageListActivity.EXTRAS_KEY, dogBreed)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun onIndexSelected(index: String) {
        viewModel.findScrollToPosition(index)
    }
}