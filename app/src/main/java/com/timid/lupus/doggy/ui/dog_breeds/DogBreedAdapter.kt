package com.timid.lupus.doggy.ui.dog_breeds

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.timid.lupus.doggy.R
import com.timid.lupus.doggy.databinding.ItemDogBreedBinding
import com.timid.lupus.doggy.ui.model.DogBreed

class DogBreedAdapter: ListAdapter<DogBreed, DogBreedAdapter.ViewHolder>(DiffUtilCallBack()) {

    private var itemClickListener: ItemClickListener? = null

    class ViewHolder(private val binding: ItemDogBreedBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(dogBreed: DogBreed, itemClickListener: ItemClickListener?) {
            binding.dogBreed = dogBreed
            binding.itemClickListener = itemClickListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_dog_breed, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), itemClickListener)
    }

    fun setItemClickListener(listener: ItemClickListener) {
        itemClickListener = listener
    }

    interface ItemClickListener {
        fun onItemClickListener(dogBreed: DogBreed)
    }
}

class DiffUtilCallBack: DiffUtil.ItemCallback<DogBreed>() {


    override fun areItemsTheSame(oldItem: DogBreed, newItem: DogBreed): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: DogBreed, newItem: DogBreed): Boolean {
        return oldItem.name == newItem.name
    }
}