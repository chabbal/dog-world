package com.timid.lupus.doggy.domain.model

data class BreedImagesDomain(
    val images: List<String>
)