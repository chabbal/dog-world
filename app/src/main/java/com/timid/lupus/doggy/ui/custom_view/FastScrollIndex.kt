package com.timid.lupus.doggy.ui.custom_view

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.animation.doOnCancel
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import com.timid.lupus.doggy.R
import kotlin.math.roundToInt

class FastScrollIndex @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    companion object {
        private val headers = listOf(
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        )
        private const val BUBBLE_ANIMATION_DURATION = 100L
    }

    private var isBottomDrawableApplied = false
    private var animator: ObjectAnimator? = null

    private var previousEmittedIndex: String = ""
    private var bubble: TextView = TextView(context).apply {
        layoutParams = FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            gravity = Gravity.END
        }
        background = ContextCompat.getDrawable(context, R.drawable.ic_bubble)
        gravity = Gravity.CENTER

        textSize = 24f
        setTextColor(ContextCompat.getColor(context, R.color.white))
        visibility = View.INVISIBLE
    }
    var indexSelectionListener: FastScrollIndexSelectionListener? = null

    var bubbleLayout: FrameLayout? = null
        set(value) {
            field = value
            bubble.y = y
            field?.addView(bubble)
        }

    init {
        orientation = VERTICAL
        addIndices()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev == null) return super.dispatchTouchEvent(ev)

        return when (ev.action) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {

                val position = getIndexViewTouchedPosition(ev.x.roundToInt(), ev.y.roundToInt())
                position?.let {
                    val currentIndexValue = headers[position]
                    if (!previousEmittedIndex.contentEquals(currentIndexValue)) {
                        showBubble(currentIndexValue)
                    }
                    scrollBubble(ev.y)
                    indexSelectionListener?.onIndexSelected(previousEmittedIndex)
                }
                true
            }
            MotionEvent.ACTION_UP -> {
                hideBubble()
                true
            }
            MotionEvent.ACTION_CANCEL -> {
                hideBubble()
                true
            }
            else -> {
                super.dispatchTouchEvent(ev)
            }
        }
    }

    private fun addIndices() {
        headers.forEach {
            addView(createIndexView(it))
        }
    }

    private fun createIndexView(name: String): View {
        val params = LayoutParams(MATCH_PARENT, 0, 1f)
        return TextView(context).apply {
            text = name
            isAllCaps = true
            textSize = 16f
            setTextColor(ContextCompat.getColor(context, R.color.black))
            layoutParams = params
            gravity = Gravity.CENTER
        }
    }

    private fun getIndexViewTouchedPosition(x: Int, y: Int): Int? {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            if (x > child.left && x < child.right && y > child.top && y < child.bottom) {
                return i
            }
        }
        return null
    }

    private fun showBubble(currentIndexValue: String) {
        previousEmittedIndex = currentIndexValue
        bubble.text = currentIndexValue
        if (bubble.visibility != VISIBLE) {
            bubble.visibility = VISIBLE
            showAnimation()
        }
    }

    private fun hideBubble() {
        hideAnimation()
    }

    private fun cancelPreviousAnimation() {
        animator?.apply {
            cancel()
            removeAllListeners()
        }
    }

    private fun showAnimation() {
        cancelPreviousAnimation()
        animator = ObjectAnimator.ofFloat(bubble, "alpha", 0f, 1f).setDuration(
            BUBBLE_ANIMATION_DURATION
        )
        animator?.start()
    }

    private fun hideAnimation() {
        cancelPreviousAnimation()
        animator = ObjectAnimator.ofFloat(bubble, "alpha", 1f, 0f).setDuration(
            BUBBLE_ANIMATION_DURATION
        )
        animator?.start()
        animator?.doOnEnd {
            bubble.visibility = INVISIBLE
            previousEmittedIndex = ""
            animator = null
        }
        animator?.doOnCancel {
            bubble.visibility = INVISIBLE
            previousEmittedIndex = ""
            animator = null
        }
    }

    private fun scrollBubble(y: Float) {
        val bubbleFutureVerticalPosition = y + bubble.height
        if (bubbleFutureVerticalPosition > height) {
            bubble.setBackgroundResource(R.drawable.ic_bubble_bottom)
            bubble.y = y - (bubbleFutureVerticalPosition - height)
            isBottomDrawableApplied = true
        } else {
            if (isBottomDrawableApplied) {
                isBottomDrawableApplied = false
                bubble.setBackgroundResource(R.drawable.ic_bubble)
            }
            if (y > 0) {
                bubble.y = y
            }
        }
    }
}
