package com.timid.lupus.doggy.ui.dog_breeds

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.timid.lupus.doggy.SchedulerProvider
import com.timid.lupus.doggy.domain.usecase.GetAllDogBreedsUseCase
import com.timid.lupus.doggy.domain.usecase.SyncAllDogBreedsUseCase
import com.timid.lupus.doggy.ui.RxJavaViewModel
import com.timid.lupus.doggy.ui.model.DogBreed
import com.timid.lupus.doggy.ui.model.toDogBreedList
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class DogBreedsViewModel(
    private val schedulerProvider: SchedulerProvider,
    private val useCase: GetAllDogBreedsUseCase,
    private val syncAllDogBreedsUseCase: SyncAllDogBreedsUseCase
): RxJavaViewModel() {



    private val dogBreeds = MutableLiveData<List<DogBreed>>()
    val dogBreedsLiveData = dogBreeds as LiveData<List<DogBreed>>

    private val fastScrollPosition = MutableLiveData<Int>()
    val fastScrollPositionLiveData = fastScrollPosition as LiveData<Int>


    init {
        syncDogBreeds()
        getAllDogBreeds()
    }

    private fun getAllDogBreeds() {
        useCase.observeAllDogBreeds()
            .distinctUntilChanged()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onNext = {
                    dogBreeds.value = it.toDogBreedList()
                },
                onError = {
                    Log.d("response", it.toString())

                }
            ).addTo(compositeDisposable)
    }

    private fun syncDogBreeds() {
        syncAllDogBreedsUseCase.syncAllDogBreeds()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onComplete = {
                    Log.d("exception", "completed query successfully")
                },
                onError = {
                    Log.d("exception", it.toString())
                }
            ).addTo(compositeDisposable)
    }

    fun findScrollToPosition(scrollIndex: String) {
        dogBreeds.value?.let {
            if (it.isNotEmpty()) {
                val position = it.indexOfFirst {
                    val firstLetterOfName = it.name.getOrNull(0)?.toString()?.capitalize()
                    firstLetterOfName == scrollIndex
                }
                if (position > -1 ) {
                    fastScrollPosition.value = position
                }
            }
        }
    }





    class Factory @Inject constructor(
        private val schedulerProvider: SchedulerProvider,
        private val useCase: GetAllDogBreedsUseCase,
        private val syncAllDogBreedsUseCase: SyncAllDogBreedsUseCase
    ): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DogBreedsViewModel(
                schedulerProvider,
                useCase,
                syncAllDogBreedsUseCase
            ) as T
        }
    }
}