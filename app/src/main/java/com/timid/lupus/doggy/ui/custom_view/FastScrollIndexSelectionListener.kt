package com.timid.lupus.doggy.ui.custom_view

interface FastScrollIndexSelectionListener {
    fun onIndexSelected(index: String)
}
