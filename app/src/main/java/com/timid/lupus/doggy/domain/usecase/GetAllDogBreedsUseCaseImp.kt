package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.model.DogBreedDomain
import com.timid.lupus.doggy.domain.repo.DogRepo
import io.reactivex.Flowable

class GetAllDogBreedsUseCaseImp(
    private val repo: DogRepo
): GetAllDogBreedsUseCase {
    override fun observeAllDogBreeds(): Flowable<List<DogBreedDomain>> {
        return repo.observeAllDogBreeds()
    }
}