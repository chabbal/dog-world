package com.timid.lupus.doggy.data.remote.responses

data class DogBreedResponse(
    val name: String,
    val subBreeds: List<String>
)
