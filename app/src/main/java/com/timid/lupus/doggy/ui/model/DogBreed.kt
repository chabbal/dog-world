package com.timid.lupus.doggy.ui.model

import com.timid.lupus.doggy.domain.model.DogBreedDomain
import java.io.Serializable

data class DogBreed(
    val name: String,
    val breedVariants: List<String>
): Serializable



fun DogBreedDomain.toDogBreed() = DogBreed(
    name,
    breedVariants
)


fun List<DogBreedDomain>.toDogBreedList() = this.map { it.toDogBreed() }

