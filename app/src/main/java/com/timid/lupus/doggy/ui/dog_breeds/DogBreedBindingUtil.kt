package com.timid.lupus.doggy.ui.dog_breeds

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("capitalizeFirstLetter")
fun TextView.capitalizeFirstLetter(name: String) {
    text = name.capitalize()
}