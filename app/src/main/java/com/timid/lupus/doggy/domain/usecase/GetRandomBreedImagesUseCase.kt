package com.timid.lupus.doggy.domain.usecase

import com.timid.lupus.doggy.domain.model.BreedImagesDomain
import io.reactivex.Single

interface GetRandomBreedImagesUseCase {
    fun getRandomBreedImages(breed: String): Single<BreedImagesDomain>
}